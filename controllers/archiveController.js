
const Course = require("../models/Course");

const Archive = require("../models/Archive");



module.exports.archiveCourse = (reqParams, reqBody) => {
	let archivedCourse = {
		isAdmin: reqBody.isActive
	};
		return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).
		then((course, error) => {
			if (error) {
				return false;
			} else {
				return true
			};
		});
};










