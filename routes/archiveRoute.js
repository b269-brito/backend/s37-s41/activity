const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");
const archiveController = require("../controllers/archiveController");
const auth = require("../auth");




router.patch("/:courseId/archive", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	archiveController.archiveCourse(userData).then(resultFromController => res.send(resultFromController));
});




module.exports = router;






